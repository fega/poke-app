const express = require('express');
const { User, Pokemon, TraineePokemon } = require('../services/');
const passport = require('passport');

const router = express.Router();

/* GET home page. */
router.use('/users', passport.authenticate('jwt', { session: false }), User.routes);
router.use('/users/me/pokemons', TraineePokemon.routes);
router.use('/pokemons', Pokemon.routes);
router.post('/login', passport.authenticate('local', { session: false }), (req, res) => {
  res.send(User.dto.single(req.user));
});
router.post('/sign-up', User.controller.complete.create);

module.exports = router;
