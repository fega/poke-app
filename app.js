const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const morganLogger = require('morgan');
const mongoose = require('mongoose');
const Router = require('./routes/index');
const config = require('config');
const { errorHandler } = require('@fega01/middleware');
const logger = require('@fega01/logger');
const helmet = require('helmet');
const cors = require('cors');
const compression = require('compression');

/**
 * INITIAL SETUP
 */
mongoose.connect(config.get('mongoose.urls'), config.get('mongoose'));

/**
 * EXPRESS SETUP
 */
const app = express();
app.use(compression(config.get('compression')));
app.use(morganLogger(config.get('morgan')));
app.use(helmet(config.get('helmet')));
app.use(cors(config.get('cors')));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/api/v1', Router);
app.use(errorHandler(logger));
module.exports = app;
