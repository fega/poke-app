const passport = require('passport');
const Local = require('passport-local').Strategy;
const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt');
const User = require('../User');
const config = require('config');

passport
  .use(new Local(config.get('passport.local'), async (email, password, done) => {
    try {
      const user = await User.model.login(email, password);
      done(null, user);
    } catch (error) {
      done(error);
    }
  }));

passport
  .use(new JwtStrategy({
    ...config.get('passport.jwt'), jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  }, async (payload, done) => {
    try {
      return done(null, payload);
    } catch (error) {
      return done(error);
    }
  }));

