const { AbstractController } = require('@fega01/proxy-controller');
const Entity = require('./model');
const Joi = require('joi');
const id = require('joi-objectid')(Joi);
const dto = require('./dto');
const { OK } = require('http-status');

Joi.id = id;
class Controller extends AbstractController {
  async create(req, res) {
    const entity = await Entity.add(req.user.id, req.body);
    res.send(dto.single(entity));
  }
  async get(req, res) {
    const entity = await Entity.get(req.params.id);
    res.send(dto.single(entity));
  }
  async list(req, res) {
    const entities = await Entity.list(req.user.id);
    res.send(dto.multiple(entities));
  }
  async remove(req, res) {
    await Entity.remove(req.params.id);
    res.sendStatus(OK);
  }
  async update(req, res) {
    await Entity.patch(req.params.id, req.body);
    res.sendStatus(OK);
  }
  get validators() {
    return {
      // create
      create: {
        body: {
          nickname: Joi.string().required().min(1).max(24),
          name: Joi.string().required(),
          level: Joi.number().integer().min(0).max(100)
            .required(),
        },
        options: {
          allowUnknownBody: false,
        },
      },
      // list
      list: {
        query: {
          limit: Joi.number().min(0).max(100),
          page: Joi.number().min(0),
        },
        options: {
          allowUnknownQuery: false,
        },
      },
    };
  }
}

module.exports = new Controller();
