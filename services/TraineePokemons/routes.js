const express = require('express');
const controller = require('./controller');
const passport = require('passport');

const router = express.Router();

/* GET home page. */
router.post('/', passport.authenticate('jwt', { session: false }), controller.complete.create);
router.get('/', passport.authenticate('jwt', { session: false }), controller.complete.list);
// router.delete('/:id', controller.complete.remove);
module.exports = router;
