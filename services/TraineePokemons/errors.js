const { BAD_REQUEST } = require('http-status');

module.exports.ERROR = {
  code: 'ERROR',
  status: BAD_REQUEST,
  message: 'An error',
};

