const admin = require('firebase-admin');
const cerConfig = require('../../config/firebase.json');
const config = require('config');
const delay = require('delay');
const Pokemon = require('../Pokemon');
const uuid = require('uuid/v1');
const { AppError, NOT_FOUND } = require('@fega01/app-error');

admin.initializeApp({
  credential: admin.credential.cert(cerConfig),
  databaseURL: 'https://pokeapp-44f41.firebaseio.com',
});
const db = admin.database();
const trainersRef = db.ref('server/pokeapp/trainers');


const add = async (userId, { name, nickname, level }) => {
  const especie = await Pokemon.model.findOne({ name });
  if (!especie) {
    throw new AppError(NOT_FOUND);
  }
  const ref = trainersRef.child(userId.toString()).child('pokemons');
  const id = uuid().replace('-', '');
  await Promise.race([
    ref.child(id).set({ name, nickname, level }),
    delay(config.get('firebase.delay')),
  ]);
  return {
    name, nickname, level, id,
  };
};
const list = async (userId) => {
  const ref = trainersRef.child(userId.toString()).child('pokemons');
  const data = await ref.once('value');
  return data.val();
};


module.exports = { list, add };
