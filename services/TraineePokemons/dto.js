const { map } = require('lodash');

class DTO {
  single(value, key) {
    return {
      id: key,
      nickname: value.nickname,
      level: value.level,
      name: value.name,
    };
  }
  multiple(object) {
    return map(object, this.single);
  }
}

module.exports = new DTO();
