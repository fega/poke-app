const express = require('express');
const controller = require('./controller');

const router = express.Router();

/* GET home page. */
router.get('/', controller.complete.list);
router.get('/:name', controller.complete.get);
module.exports = router;
