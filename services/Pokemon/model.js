const mongoose = require('mongoose');
const { AppError, NOT_FOUND } = require('@fega01/app-error');

const Schema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  types: [String],
  id: String,
  url: String,
  hp: Number,
  attack: Number,
  defense: Number,
  spAttack: Number,
  spDefense: Number,
  speed: Number,
}, { timestamps: true });

Schema.statics.list = async function list(query, { page = 0, limit = 10 } = {}) {
  const entities = await this.find(query).skip(page * limit).limit(limit);
  return entities.map(obj => obj.toObject());
};
Schema.statics.get = async function get(name) {
  const entity = await this.findOne({ name });
  if (!entity) throw new AppError(NOT_FOUND);
  return entity.toObject();
};
const Model = mongoose.model('Pokemon', Schema);

module.exports = Model;
