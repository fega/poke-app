const { AbstractController } = require('@fega01/proxy-controller');
const Entity = require('./model');
const Joi = require('joi');
const id = require('joi-objectid')(Joi);
const dto = require('./dto');

Joi.id = id;
class Controller extends AbstractController {
  async get(req, res) {
    const entity = await Entity.get(req.params.name);
    res.send(dto.single(entity));
  }
  async list(req, res) {
    const entities = await Entity.list({}, req.query);
    res.send(dto.multiple(entities));
  }
  get validators() {
    return {
      // list
      list: {
        query: {
          limit: Joi.number().min(0).max(1000),
          page: Joi.number().min(0),
        },
        options: {
          allowUnknownQuery: false,
        },
      },
      // get
      get: {
        params: {
          name: Joi.string().required(),
        },
      },
    };
  }
}

module.exports = new Controller();
