const { AbstractDto } = require('@fega01/dto');

class DTO extends AbstractDto {
  single(object) {
    return {
      id: object._id,
      name: object.name,
      types: object.types,
      hp: object.hp,
      attack: object.attack,
      defense: object.defense,
      spAttack: object.spAttack,
      spDefense: object.spDefense,
      speed: object.speed,
    };
  }
}

module.exports = new DTO();
