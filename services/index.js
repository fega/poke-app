require('./Auth');
const User = require('./User');
const TraineePokemon = require('./TraineePokemons');
const Pokemon = require('./Pokemon');

module.exports = {
  User,
  Pokemon,
  TraineePokemon,
};

