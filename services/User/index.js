const controller = require('./controller');
const routes = require('./routes');
const model = require('./model');
const errors = require('./errors');
const dto = require('./dto');

module.exports = {
  controller, routes, model, errors, dto,
};
