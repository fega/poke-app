const { AbstractDto } = require('@fega01/dto');

class DTO extends AbstractDto {
  single(user) {
    return {
      id: user._id,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      jwt: user.jwt,
    };
  }
}

module.exports = new DTO();
