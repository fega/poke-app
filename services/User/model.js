const mongoose = require('mongoose');
const { AppError, NOT_FOUND } = require('@fega01/app-error');
const { PASSWORD_AND_CONFIRMATION_NOT_EQUAL, USER_OR_PASSWORD_INVALID, USER_ALREADY_EXIST } = require('./errors');
const { hash, compare } = require('bcrypt');
const { clone } = require('lodash');
const config = require('config');
const jsonwebtoken = require('jsonwebtoken');

const sign = payload => new Promise((resolve, reject) => {
  jsonwebtoken.sign(payload, config.get('passport.jwt.secretOrKey'), {
    expiresIn: '1 hour',
    audience: config.get('passport.jwt.audience'),
    issuer: config.get('passport.jwt.issuer'),
  }, (err, token) => {
    if (err) reject(err);
    else resolve(token);
  });
});
const Schema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  role: { type: [String], default: 'trainer' },
  password: String,
  email: { type: String, required: true },
}, { timestamps: true });

Schema.statics.add = async function add({ password, confirmation, email }) {
  if (password !== confirmation) {
    throw new AppError(PASSWORD_AND_CONFIRMATION_NOT_EQUAL);
  }
  const user = await this.findOne({ email });
  if (user) {
    throw new AppError(USER_ALREADY_EXIST);
  }
  const data = clone({ password, confirmation, email });
  data.password = await hash(data.password, config.get('bcrypt.rounds'));
  const newUser = await this.create(data);
  return newUser.toObject();
};
Schema.statics.list = async function list(query, { page = 0, limit = 10 } = {}) {
  const users = await this.find({}).skip(page * limit).limit(limit);
  return users.map(obj => obj.toObject());
};
Schema.statics.patch = async function update(id, { firstName, lastName }) {
  await this.findByIdAndUpdate(id, { $set: { firstName, lastName } });
};
Schema.statics.get = async function get(id) {
  const user = await this.findById(id);
  if (!user) throw new AppError(NOT_FOUND);
  return user.toObject();
};
Schema.statics.login = async function update(email, password) {
  const user = await this.findOne({ email });
  if (!user) throw new AppError(USER_OR_PASSWORD_INVALID);
  if (!compare(password, user.password)) throw new AppError(USER_OR_PASSWORD_INVALID);
  const userObj = user.toObject();
  const jwt = await sign({
    id: userObj._id.toString(),
    firstName: userObj.firstName,
  });
  return { ...userObj, jwt };
};
const Model = mongoose.model('User', Schema);

module.exports = Model;
