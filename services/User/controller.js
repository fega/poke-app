const { AbstractController } = require('@fega01/proxy-controller');
const User = require('./model');
const Joi = require('joi');
const id = require('joi-objectid')(Joi);
const dto = require('./dto');
const { OK } = require('http-status');

Joi.id = id;
class Controller extends AbstractController {
  async create(req, res) {
    const user = await User.add(req.body);
    res.send(dto.single(user));
  }
  async get(req, res) {
    const user = await User.get(req.params.id);
    res.send(dto.single(user));
  }
  async list(req, res) {
    const users = await User.list();
    res.send(dto.multiple(users));
  }
  async update(req, res) {
    await User.patch(req.user.id, req.body);
    res.sendStatus(OK);
  }
  get validators() {
    return {
      // create
      create: {
        body: {
          password: Joi.string().min(8).max(145).required(),
          confirmation: Joi.string().min(8).max(145).required(),
          email: Joi.string().email().required(),
        },
        options: {
          allowUnknownBody: false,
        },
      },
      // list
      list: {
        query: {
          limit: Joi.number().min(0).max(1000),
          page: Joi.number().min(0),
        },
        options: {
          allowUnknownQuery: false,
        },
      },
      // get
      get: {
        params: {
          id: Joi.id().required(),
        },
      },
      // update
      update: {
        params: {
          id: Joi.id().required(),
        },
        body: {
          firstName: Joi.string().required(),
        },
        options: {
          allowUnknownBody: false,
        },
      },
    };
  }
}

module.exports = new Controller();
