const { BAD_REQUEST, CONFLICT } = require('http-status');

module.exports.PASSWORD_AND_CONFIRMATION_NOT_EQUAL = {
  code: 'PASSWORD_AND_CONFIRMATION_NOT_EQUAL',
  status: BAD_REQUEST,
  message: 'Password and confirmation should be equal',
};

module.exports.USER_OR_PASSWORD_INVALID = {
  code: 'USER_OR_PASSWORD_INVALID',
  status: BAD_REQUEST,
  message: 'User or password is invalid',
};

module.exports.USER_ALREADY_EXIST = {
  code: 'USER_ALREADY_EXIST',
  status: CONFLICT,
  message: 'User already exist',
};

