const express = require('express');
const controller = require('./controller');

const router = express.Router();

/* GET home page. */
router.get('/', controller.complete.list);
router.get('/:id', controller.complete.get);
router.patch('/me', controller.complete.update);
module.exports = router;
