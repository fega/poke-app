const { model: User } = require('../services/User');
const { generate } = require('randomstring');

module.exports.generateEmail = () => `${generate()}@mail.com`;

module.exports.createUser = (obj = {}) => User.create({
  email: `${module.exports.generateEmail()}`,
  ...obj,
});

module.exports.findUser = async query => User.findOne(query).lean(true);

