/* eslint-env node, mocha */
const chai = require('chai');
const request = require('supertest');
const app = require('../app');
const {
  BAD_REQUEST, OK, NOT_FOUND, UNAUTHORIZED, CONFLICT,
} = require('http-status');
const mongoose = require('mongoose');
const { createUser, generateEmail } = require('./util');

const a = chai.assert;
after(() => {
  mongoose.models = {};
  mongoose.modelSchemas = {};
});


suite('GET /pokemons');
test('OK', async () => {
  const r = await request(app)
    .get('/api/v1/pokemons/');

  a.equal(r.status, OK);
  a.equal(r.body.length, 10);
  a.ok(r.body[0].id);
  a.equal(r.body[0].name, 'bulbasaur');
  a.equal(r.body[0].types[0], 'grass');
});
test('OK, limit', async () => {
  const r = await request(app)
    .get('/api/v1/pokemons/?limit=5&page=2');

  a.equal(r.status, OK);
  a.equal(r.body.length, 5);
  a.ok(r.body[0].id);
  a.equal(r.body[0].name, 'metapod');
  a.equal(r.body[0].types[0], 'bug');
});


suite('GET /pokemons/:name');
test('OK', async () => {
  const r = await request(app)
    .get('/api/v1/pokemons/pikachu');

  a.equal(r.status, OK);
  a.equal(r.body.name, 'pikachu');
  a.equal(r.body.types[0], 'electric');
});
test('NOT_FOUND', async () => {
  const r = await request(app)
    .get('/api/v1/pokemons/chikorita');

  a.equal(r.status, NOT_FOUND);
});


suite('POST /sign-up');
test('BAD_REQUEST, missing body', async () => {
  const r = await request(app)
    .post('/api/v1/sign-up');
  a.equal(r.status, BAD_REQUEST);
  a.equal(r.body.errors.body, '"password" is required,"confirmation" is required,"email" is required');
});
test('BAD_REQUEST, password too weak', async () => {
  const r = await request(app)
    .post('/api/v1/sign-up').send({
      email: generateEmail(),
      password: 'abc',
      confirmation: 'abc',
    });
  a.equal(r.status, BAD_REQUEST);
  a.equal(r.body.errors.body, '"password" length must be at least 8 characters long,"confirmation" length must be at least 8 characters long');
});
test('BAD_REQUEST, user already exist', async () => {
  const user = await createUser();
  const r = await request(app)
    .post('/api/v1/sign-up').send({
      email: user.email,
      password: '12345678910',
      confirmation: '12345678910',
    });
  a.equal(r.status, CONFLICT);
});
test('OK', async () => {
  const r = await request(app)
    .post('/api/v1/sign-up').send({
      email: generateEmail(),
      password: '12345678910',
      confirmation: '12345678910',
    });
  a.equal(r.status, OK);
});

suite('POST /login');
test('BAD_REQUEST, missing body', async () => {
  const r = await request(app)
    .post('/api/v1/login');
  a.equal(r.status, BAD_REQUEST);
});
test('BAD_REQUEST, user does not exist', async () => {

});
test('BAD_REQUEST, wrong password');
test('OK', async () => {
  const user = await createUser({ password: 'hola' });

  const r = await request(app)
    .post('/api/v1/login')
    .send({
      email: user.email,
      password: 'hola',
    });
  a.equal(r.status, OK);
  a.ok(r.body);
  a.equal(r.body.email, user.email);
  a.notOk(r.body.password, "YOU'RE SENDING THE PASSWORD!");
  a.ok(r.body.jwt);
});

suite('POST /user/me/pokemons');
test('NOT_AUTHENTICATED, User is not authenticated', async () => {
  const r = await request(app)
    .post('/api/v1/users/me/pokemons');
  a.equal(r.status, UNAUTHORIZED);
});
test('BAD_REQUEST, missing body', async function () { // eslint-disable-line
  this.timeout(5000);
  // setup
  const user = await createUser({ password: 'hola' });
  const r0 = await request(app).post('/api/v1/login')
    .send({ email: user.email, password: 'hola' });
  // use
  const r = await request(app)
    .post('/api/v1/users/me/pokemons')
    .set('Authorization', `Bearer ${r0.body.jwt}`);
  a.equal(r.status, BAD_REQUEST);
});
test('BAD_REQUEST, unkown pokemon', async function () { // eslint-disable-line
  this.timeout(5000);
  // setup
  const user = await createUser({ password: 'hola' });
  const r0 = await request(app).post('/api/v1/login')
    .send({ email: user.email, password: 'hola' });
  // use
  const r = await request(app)
    .post('/api/v1/users/me/pokemons')
    .set('Authorization', `Bearer ${r0.body.jwt}`)
    .send({ nickname: 'tatacoa', name: 'nikachu', level: 20 });
  a.equal(r.status, NOT_FOUND);
});
test('OK', async function () { // eslint-disable-line
  this.timeout(5000);
  // setup
  const user = await createUser({ password: 'hola' });
  const r0 = await request(app).post('/api/v1/login')
    .send({ email: user.email, password: 'hola' });
  // use
  const r = await request(app)
    .post('/api/v1/users/me/pokemons')
    .set('Authorization', `Bearer ${r0.body.jwt}`)
    .send({ nickname: 'tatacoa', name: 'pikachu', level: 20 });
  a.equal(r.status, OK);
});

suite('GET /user/me/pokemons');
test('NOT_AUTHENTICATED, User is not authenticated', async () => {
  const r = await request(app)
    .get('/api/v1/users/me/pokemons');
  a.equal(r.status, UNAUTHORIZED);
});
test('OK', async function () { // eslint-disable-line
  this.timeout(8000);
  // setup...
  // usually I would create new object in db instead of call endpoint, but in order to save  time...
  const user = await createUser({ password: 'hola' });
  const r0 = await request(app).post('/api/v1/login')
    .send({ email: user.email, password: 'hola' });
  await request(app)
    .post('/api/v1/users/me/pokemons')
    .set('Authorization', `Bearer ${r0.body.jwt}`)
    .send({ nickname: 'tatacoa', name: 'pikachu', level: 20 });

  // use
  const r = await request(app)
    .get('/api/v1/users/me/pokemons')
    .set('Authorization', `Bearer ${r0.body.jwt}`)
    .send({ nickname: 'tatacoa', name: 'pikachu', level: 20 });
  a.equal(r.status, OK);
  a.ok(r.body[0].id);
  a.equal(r.body[0].nickname, 'tatacoa');
  a.equal(r.body[0].name, 'pikachu');
  a.equal(r.body[0].level, 20);
});
