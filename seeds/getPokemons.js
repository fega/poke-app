const cheerio = require('cheerio'); // eslint-disable-line
const axios = require('axios'); // eslint-disable-line
const { Pokemon } = require('../services');
const mongoose = require('mongoose');
const config = require('config');
const { uniqBy } = require('lodash');
const logger = require('pino')();

mongoose.connect(config.get('mongoose.urls'), config.get('mongoose'));


const getPokemon = (tableItem) => {
  const $ = cheerio.load(tableItem);
  const tds = $('td');
  const types = cheerio.load(tds[2])('.type-icon').text().match(/[A-Z][a-z]+/g).map(str => str.toLocaleLowerCase());

  const pokemon = {
    id: cheerio.load(tds[0]).text(),
    name: $('.ent-name').text().toLowerCase(),
    url: $('.ent-name').attr('href'),
    hp: cheerio.load(tds[3]).text(),
    attack: cheerio.load(tds[4]).text(),
    defense: cheerio.load(tds[5]).text(),
    spAttack: cheerio.load(tds[6]).text(),
    spDefense: cheerio.load(tds[7]).text(),
    speed: cheerio.load(tds[8]).text(),
    types,
  };
  return pokemon;
};
const getPokemons = (tables) => {
  const pokemons = [];
  for (let i = 0; i < 184; i += 1) {
    pokemons.push(getPokemon(tables.get(i)));
  }
  return uniqBy(pokemons, 'id');
};
const seed = async () => {
  const body = await axios.get('https://pokemondb.net/pokedex/all');
  const { data } = body;
  const $ = cheerio.load(data);
  const tables = $('#pokedex tbody tr');

  const pokemons = getPokemons(tables);
  return pokemons;
};
const main = async () => {
  console.log('start');
  const pokemons = await seed();
  console.log('inserting');
  await Pokemon.model.insertMany(pokemons);
  console.log('close');
  mongoose.connection.close();
  process.exit();
};

main().catch(logger.error);
